# SIMPUL API
a Backend API for simpul web and simpul apps

## API DOCUMENTATION
api documentation is available at website root http://localhost:8000

## How To Install
- pull/clone this project
- set up a new `venv`
- install required package `pip install -r requirements.txt`
- copy `opt_env.cnv.example` to `opt_env.cnv` and config your mysql database
- run migration `python manage.py migrate`
- start the development server `python manage.py runserver`

## ADD DATA
1. Address Dataset
    - cd `dataset`
    - `mysql -u root`
    - use your databases
    - type all command below
    ```sql
    LOAD DATA LOCAL INFILE 'province.csv' INTO TABLE datul_provinsi FIELDS TERMINATED by "," LINES TERMINATED BY "\n";
    
    LOAD DATA LOCAL INFILE 'postal.csv' INTO TABLE datul_daerah FIELDS TERMINATED by "," LINES TERMINATED BY "\n";
    ```
2. Dummy Peserta Dataset
    ```sql
    LOAD DATA LOCAL INFILE 'p2tel.csv' INTO TABLE datul_p2tel FIELDS TERMINATED by "," LINES TERMINATED BY "\n";

    LOAD DATA LOCAL INFILE 'peserta.csv' INTO TABLE datul_peserta FIELDS TERMINATED by "," LINES TERMINATED BY "\n";

    LOAD DATA LOCAL INFILE 'pmp.csv' INTO TABLE datul_pmp FIELDS TERMINATED by "," LINES TERMINATED BY "\n";
    ```