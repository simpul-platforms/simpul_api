from django.urls import path, re_path

from . import views


app_name = 'datul'
urlpatterns = [
    # path('', views.index, name='index'),
    re_path(r'^peserta/$',
            views.PesertaListView.as_view(), name='peserta_list'),
    re_path(r'^peserta/(?P<pk>[0-9]+)/$',
            views.PesertaRowView.as_view()),
    re_path(r'^peserta/status/(?P<status>[0-9]+)/$',
            views.PesertaStatusView.as_view()),
    re_path(r'^pmp/$',
            views.PMPListView.as_view(), name='pmp_list'),
    re_path(r'^pmp/(?P<pk>[0-9]+)/$',
            views.PMPRowView.as_view()),
    re_path(r'^provinsi/$',
            views.ProvinsiListView.as_view(), name='prov_list'),
    re_path(r'^provinsi/(?P<pk>[0-9]+)/$',
            views.ProvinsiRowView.as_view()),
    re_path(r'^daerah/$',
            views.DaerahListView.as_view(), name='drh_list'),
    re_path(r'^daerah/(?P<pk>[0-9]+)/$',
            views.DaerahRowView.as_view()),
    re_path(r'^daerah/prov/(?P<id_prov>[0-9]+)/$',
            views.DaerahProvView.as_view()),
    re_path(r'^daerah/kota/(?P<kota>[\w+\+?]+)/$',
            views.DaerahKotaView.as_view()),
    re_path(r'^daerah/camat/(?P<camat>[\w+\+?]+)/$',
            views.DaerahCamatView.as_view()),
    re_path(r'^daerah/desa/(?P<desa>[\w+\+?]+)/$',
            views.DaerahDesaView.as_view()),
    re_path(r'^daerah/pos/(?P<pos>\w+)/$',
            views.DaerahPostalView.as_view()),
    re_path(r'^p2tel/$',
            views.P2TELListView.as_view(), name='p2tel_list'),
    re_path(r'^p2tel/(?P<pk>[0-9]+)/$',
            views.P2TELRowView.as_view()),
]
