from rest_framework import generics, permissions, status
from rest_framework.response import Response
from .serializers import (PesertaSerializer, PMPSerializer,
                          P2TELSerializer, P2TEL_MemberSerializer,
                          P2TEL_WilayahSerializer, KontakSerializer,
                          DatulSerializer, DokumenSerializer,
                          PMP_TempSerializer, ProvinsiSerializer,
                          DaerahSerializer)
from .models import (Peserta, PMP,
                     P2TEL, P2TEL_Member,
                     P2TEL_Wilayah, Kontak,
                     Datul, Dokumen, PMP_Temp,
                     Provinsi, Daerah)


class PesertaListView(generics.ListCreateAPIView):
    queryset = Peserta.objects.all()
    serializer_class = PesertaSerializer
    permission_classes = (permissions.IsAuthenticated,)


class PesertaRowView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Peserta.objects.all()
    serializer_class = PesertaSerializer
    permission_classes = (permissions.IsAuthenticated,)


class PMPListView(generics.ListCreateAPIView):
    queryset = PMP.objects.all()
    serializer_class = PMPSerializer
    permission_classes = (permissions.IsAuthenticated,)


class PMPRowView(generics.RetrieveUpdateDestroyAPIView):
    queryset = PMP.objects.all()
    serializer_class = PMPSerializer
    permission_classes = (permissions.IsAuthenticated,)


class KontakListView(generics.ListCreateAPIView):
    pagination_class = None
    queryset = Kontak.objects.all()
    serializer_class = KontakSerializer
    permission_classes = (permissions.IsAuthenticated,)


class KontakRowView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Kontak.objects.all()
    serializer_class = KontakSerializer
    permission_classes = (permissions.IsAuthenticated,)


class P2TELListView(generics.ListCreateAPIView):
    queryset = P2TEL.objects.all()
    serializer_class = P2TELSerializer
    permission_classes = (permissions.IsAuthenticated,)


class P2TELRowView(generics.RetrieveUpdateDestroyAPIView):
    queryset = P2TEL.objects.all()
    serializer_class = P2TELSerializer
    permission_classes = (permissions.IsAuthenticated,)


class DatulListView(generics.ListCreateAPIView):
    queryset = Datul.objects.all()
    serializer_class = DatulSerializer
    permission_classes = (permissions.IsAuthenticated,)


class DatulRowView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Datul.objects.all()
    serializer_class = DatulSerializer
    permission_classes = (permissions.IsAuthenticated,)


class ProvinsiListView(generics.ListCreateAPIView):
    pagination_class = None
    queryset = Provinsi.objects.all()
    serializer_class = ProvinsiSerializer
    permission_classes = (permissions.IsAuthenticated,)


class ProvinsiRowView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Provinsi.objects.all()
    serializer_class = ProvinsiSerializer
    permission_classes = (permissions.IsAuthenticated,)


class DaerahListView(generics.ListCreateAPIView):
    pagination_class = None
    queryset = Daerah.objects.all()
    serializer_class = DaerahSerializer
    permission_classes = (permissions.IsAuthenticated,)


class DaerahRowView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Daerah.objects.all()
    serializer_class = DaerahSerializer
    permission_classes = (permissions.IsAuthenticated,)


class DaerahProvView(generics.ListAPIView):
    pagination_class = None
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = DaerahSerializer

    def get_queryset(self):
        provinsi = self.kwargs['id_prov']
        return Daerah.objects.filter(provinsi_id=provinsi)


class DaerahKotaView(generics.ListAPIView):
    pagination_class = None
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = DaerahSerializer

    def get_queryset(self):
        kota = self.kwargs['kota']
        kota = kota.replace('+', ' ')
        return Daerah.objects.filter(kota=kota)


class DaerahCamatView(generics.ListAPIView):
    pagination_class = None
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = DaerahSerializer

    def get_queryset(self):
        camat = self.kwargs['camat']
        camat = camat.replace('+', ' ')
        return Daerah.objects.filter(kecamatan=camat)


class DaerahDesaView(generics.ListAPIView):
    pagination_class = None
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = DaerahSerializer

    def get_queryset(self):
        desa = self.kwargs['desa']
        desa = desa.replace('+', ' ')
        return Daerah.objects.filter(kelurahan=desa)


class DaerahPostalView(generics.ListAPIView):
    pagination_class = None
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = DaerahSerializer

    def get_queryset(self):
        postal = self.kwargs['pos']
        return Daerah.objects.filter(kode_pos=postal)


class PesertaStatusView(generics.ListAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = PesertaSerializer

    def get_queryset(self):
        status = self.kwargs['status']
        return Peserta.objects.filter(status=status)
