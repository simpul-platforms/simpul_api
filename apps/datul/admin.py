from django.contrib import admin
from .models import (
    Provinsi, Daerah, P2TEL, P2TEL_Member,
    P2TEL_Wilayah, Peserta, PMP, Kontak, Datul
    )


admin.site.register(Provinsi)
admin.site.register(Daerah)
admin.site.register(P2TEL)
admin.site.register(P2TEL_Member)
admin.site.register(P2TEL_Wilayah)
admin.site.register(Peserta)
admin.site.register(PMP)
admin.site.register(Kontak)
admin.site.register(Datul)
