from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.db import models


class Provinsi(models.Model):
    nama_provinsi = models.CharField(max_length=50)

    def __str__(self):
        return self.nama_provinsi


class Daerah(models.Model):
    kelurahan = models.CharField(max_length=50)
    kecamatan = models.CharField(max_length=50)
    kota = models.CharField(max_length=50)
    provinsi = models.ForeignKey(
        Provinsi,
        on_delete=models.CASCADE,
    )
    kode_pos = models.CharField(max_length=10)

    def __str__(self):
        return self.kelurahan


class P2TEL(models.Model):
    nama_pc = models.CharField(max_length=100)
    user_pc = models.ForeignKey(
        get_user_model(),
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )
    alamat = models.TextField()
    Kontak = models.TextField()

    def __str__(self):
        return self.nama_pc


class P2TEL_Wilayah(models.Model):
    id_p2tel = models.ForeignKey(
        P2TEL,
        on_delete=models.CASCADE,
    )
    id_daerah = models.ForeignKey(
        Daerah,
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.id_p2tel + ' ' + self.id_daerah


class Peserta(models.Model):
    nik = models.CharField(max_length=10)
    nama = models.CharField(max_length=100)
    p2tel = models.ForeignKey(
        P2TEL,
        on_delete=models.CASCADE,
    )
    status = models.CharField(max_length=50)
    penyebab = models.CharField(max_length=100)
    tanggal_penyebab = models.DateField(null=True, blank=True)

    def __str__(self):
        return self.nik


class P2TEL_Member(models.Model):
    peserta = models.ForeignKey(
        Peserta,
        on_delete=models.CASCADE,
    )
    jabatan = models.CharField(max_length=20)

    def __str__(self):
        return self.peserta


class PMP(models.Model):
    user = models.ForeignKey(
        get_user_model(),
        on_delete=models.CASCADE,
        blank=True,
        null=True
    )
    peserta = models.ForeignKey(
        Peserta,
        on_delete=models.CASCADE,
    )
    id_p2tel = models.ForeignKey(
        P2TEL,
        on_delete=models.CASCADE,
    )
    nama = models.CharField(max_length=100)
    awn = models.SmallIntegerField()
    hubungan = models.CharField(max_length=50, blank=True)
    status_hidup = models.BooleanField()
    ktp = models.CharField(max_length=20, blank=True)
    npwp = models.CharField(max_length=20, blank=True)
    agama = models.CharField(max_length=20, blank=True)
    goldar = models.CharField(max_length=5, blank=True)
    jk = models.CharField(max_length=5)
    tempat_lahir = models.CharField(max_length=50)
    tanggal_lahir = models.DateField()
    jalan = models.CharField(max_length=100, blank=True)
    komplek = models.CharField(max_length=100, blank=True)
    blok = models.CharField(max_length=20, blank=True)
    no = models.CharField(max_length=20, blank=True)
    rt = models.CharField(max_length=20, blank=True)
    rw = models.CharField(max_length=20, blank=True)
    provinsi = models.CharField(max_length=50, blank=True)
    kota = models.CharField(max_length=50, blank=True)
    kecamatan = models.CharField(max_length=50, blank=True)
    kelurahan = models.CharField(max_length=50, blank=True)
    kodepos = models.CharField(max_length=50, blank=True)

    def __str__(self):
        return self.nama


class Kontak(models.Model):
    pmp = models.ForeignKey(
        PMP,
        on_delete=models.CASCADE,
    )
    kontak = models.CharField(max_length=100)

    def __str__(self):
        return self.kontak


class Datul(models.Model):
    peserta = models.ForeignKey(
        Peserta,
        on_delete=models.CASCADE,
    )
    user_pendatul = models.ForeignKey(
        get_user_model(),
        on_delete=models.CASCADE,
    )
    tanggal_datul = models.DateField()
    tanggal_silaturahim = models.DateField()
    jenis_silaturahim = models.CharField(max_length=20)
    catatan_datul = models.TextField()

    def __str__(self):
        return self.peserta


class Dokumen(models.Model):
    id_datul = models.ForeignKey(
        Datul,
        on_delete=models.CASCADE,
    )
    detail = models.TextField()
    file = models.FileField()

    def __str__(self):
        return self.detail


class PMP_Temp(models.Model):
    user = models.ForeignKey(
        get_user_model(),
        on_delete=models.CASCADE,
        blank=True,
        null=True
    )
    peserta = models.ForeignKey(
        Peserta,
        on_delete=models.CASCADE,
    )
    id_p2tel = models.ForeignKey(
        P2TEL,
        on_delete=models.CASCADE,
    )
    nama = models.CharField(max_length=100)
    awn = models.SmallIntegerField()
    hubungan = models.CharField(max_length=50, blank=True)
    status_hidup = models.BooleanField()
    ktp = models.CharField(max_length=20, blank=True)
    npwp = models.CharField(max_length=20, blank=True)
    agama = models.CharField(max_length=20, blank=True)
    goldar = models.CharField(max_length=5, blank=True)
    jk = models.CharField(max_length=5)
    tempat_lahir = models.CharField(max_length=50)
    tanggal_lahir = models.DateField()
    jalan = models.CharField(max_length=100, blank=True)
    komplek = models.CharField(max_length=100, blank=True)
    blok = models.CharField(max_length=20, blank=True)
    no = models.CharField(max_length=20, blank=True)
    rt = models.CharField(max_length=20, blank=True)
    rw = models.CharField(max_length=20, blank=True)
    provinsi = models.CharField(max_length=50, blank=True)
    kota = models.CharField(max_length=50, blank=True)
    kecamatan = models.CharField(max_length=50, blank=True)
    kelurahan = models.CharField(max_length=50, blank=True)
    kodepos = models.CharField(max_length=50, blank=True)

    def __str__(self):
        return self.user
