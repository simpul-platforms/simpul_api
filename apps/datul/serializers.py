from rest_framework import serializers
from .models import (Peserta, PMP, 
                     P2TEL, P2TEL_Member,
                     P2TEL_Wilayah, Kontak,
                     Datul, Dokumen, PMP_Temp,
                     Daerah, Provinsi) 


class PesertaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Peserta
        fields = '__all__'


class PMPSerializer(serializers.ModelSerializer):
    class Meta:
        model = PMP
        fields = '__all__'


class P2TELSerializer(serializers.ModelSerializer):
    class Meta:
        model = P2TEL
        fields = '__all__'


class P2TEL_MemberSerializer(serializers.ModelSerializer):
    class Meta:
        model = P2TEL_Member
        fields = '__all__'


class P2TEL_WilayahSerializer(serializers.ModelSerializer):
    class Meta:
        model = P2TEL_Wilayah
        fields = '__all__'


class KontakSerializer(serializers.ModelSerializer):
    class Meta:
        model = Kontak
        fields = '__all__'


class DatulSerializer(serializers.ModelSerializer):
    class Meta:
        model = Datul
        fields = '__all__'


class DokumenSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dokumen
        fields = '__all__'


class PMP_TempSerializer(serializers.ModelSerializer):
    class Meta:
        model = PMP
        fields = '__all__'


class ProvinsiSerializer(serializers.ModelSerializer):
    class Meta:
        model = Provinsi
        fields = '__all__'


class DaerahSerializer(serializers.ModelSerializer):
    class Meta:
        model = Daerah
        fields = '__all__'
