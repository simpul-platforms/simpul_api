from django.urls import path, re_path
from rest_framework_jwt.views import refresh_jwt_token
from .views import RegisterUsersView, LoginView, UserView, UserNameView

app_name = 'user'
urlpatterns = [
    path('api-token-refresh/', refresh_jwt_token, name='new_token'),
    path('auth/login/', LoginView.as_view(), name='user_login'),
    path('auth/register/', RegisterUsersView.as_view(), name='user_regis'),
    re_path(r'^user/(?P<nik>\w+)/$',
            UserView.as_view()),
    re_path(r'^user/id/(?P<username>\w+)/$',
            UserNameView.as_view()),
    # path('songs/', ListSongsView.as_view(), name="songs-all")
]
