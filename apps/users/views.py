from datetime import datetime

from django.contrib.auth import authenticate, login
from django.contrib.auth import get_user_model
from rest_framework import generics, permissions, status
from rest_framework.response import Response
from rest_framework_jwt.settings import api_settings

from .serializers import TokenSerializer, RegisterSerializer, UserSerializer

from apps.datul.models import Kontak, PMP

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
User = get_user_model()


class LoginView(generics.CreateAPIView):
    """
    POST auth/login/
    """
    # This permission class will overide the global permission
    # class setting
    permission_classes = (permissions.AllowAny,)
    http_method_names = ['post', ]

    def post(self, request, *args, **kwargs):
        nik = request.data.get("nik", "")
        tanggal_lahir = request.data.get("tanggal_lahir", "")
        username = nik + '_' + tanggal_lahir
        password = request.data.get("password", "")
        user = authenticate(request, username=username, password=password)
        if user is not None:
            # login saves the user’s ID in the session,
            # using Django’s session framework.
            login(request, user)
            serializer = TokenSerializer(data={
                # using drf jwt utility functions to generate a token
                "token": jwt_encode_handler(
                    jwt_payload_handler(user)
                )})
            serializer.is_valid()
            return Response(serializer.data)
        else:
            return Response(
                data={
                    "message": "username or password is wrong"
                },
                status=status.HTTP_400_BAD_REQUEST
            )
        return Response(status=status.HTTP_401_UNAUTHORIZED)


class RegisterUsersView(generics.CreateAPIView):
    """
    POST auth/register/
    """
    permission_classes = (permissions.AllowAny,)
    http_method_names = ['post', ]

    def post(self, request, *args, **kwargs):
        serializer = RegisterSerializer(
            data=request.data
        )
        nik = request.data.get('nik', '')
        password = request.data.get('password', '')
        tanggal_lahir = request.data.get('tanggal_lahir', '00000000')
        email = request.data.get('email', '')
        kontak_hp = request.data.get('kontak', '')

        username = nik + '_' + tanggal_lahir
        if serializer.is_valid(raise_exception=True):
            tl = datetime.strptime(tanggal_lahir, '%d%m%Y')
            pmps = PMP.objects.filter(
                peserta__nik=nik,
            )
            if not pmps:
                return Response(
                    data={
                        "message": "username or tanggal_lahir invalid"
                    },
                    status=status.HTTP_400_BAD_REQUEST
                )
            for pmp in pmps:
                if pmp.tanggal_lahir == tl.date():
                    if not pmp.user:
                        new_user = User.objects.create_user(
                            username=username,
                            password=password,
                            email=email,
                            nik=nik,
                        )
                        user = User.objects.get(id=new_user.id)
                        pmp.user = user
                        pmp.save()
                        Kontak.objects.create(
                            kontak=kontak_hp,
                            pmp=pmp
                        )
                        return Response(status=status.HTTP_201_CREATED)
            mess = "account already exist, or pmp is not registered"
            return Response(
                data={
                    "message": mess
                },
                status=status.HTTP_400_BAD_REQUEST
            )
        return Response(
            data={
                serializer.errors
            },
            status=status.HTTP_400_BAD_REQUEST
        )


class UserView(generics.ListAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = UserSerializer

    def get_queryset(self):
        nik = self.kwargs['nik']
        return User.objects.filter(nik=nik)


class UserNameView(generics.ListAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = UserSerializer

    def get_queryset(self):
        username = self.kwargs['username']
        return User.objects.filter(username=username)
