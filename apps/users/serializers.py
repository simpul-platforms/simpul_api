from django.contrib.auth import get_user_model

from rest_framework import serializers
from datetime import datetime

User = get_user_model()


class TokenSerializer(serializers.Serializer):
    """
    This serializer serializes the token data
    """
    token = serializers.CharField(max_length=255)


class RegisterSerializer(serializers.ModelSerializer):
    password2 = serializers.CharField(max_length=200)
    kontak = serializers.CharField(max_length=30)
    tanggal_lahir = serializers.CharField(max_length=20)
    
    def validate(self, data):
        if data['password'] != data['password2']:
            raise serializers.ValidationError("password is not match")
        if not data['kontak'].strip():
            raise serializers.ValidationError("Phone number is required")
        if not data['tanggal_lahir'].strip():
            raise serializers.ValidationError("tanggal lahir is required")
        else:
            try:
                datetime.strptime(data['tanggal_lahir'], '%d%m%Y')
            except:
                raise serializers.ValidationError("tanggal lahir tidak valid")
        return data

    class Meta:
        model = User
        fields = (
            'nik', 'password',
            'password2', 'email',
            'kontak', 'tanggal_lahir'
        )


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        exclude = ['password', 'is_staff', 'is_active', 'is_superuser']
