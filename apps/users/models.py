from django.contrib.auth.models import AbstractUser
from django.db import models


class CustomUser(AbstractUser):
    nik = models.CharField(max_length=10, blank=True)
    foto_profil = models.TextField(blank=True)

    first_name = None
    last_name = None

    def __str__(self):
        return self.username
