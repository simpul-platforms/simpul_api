from django.contrib import admin
from .models import Berita, Data_History, Info

admin.site.register(Berita)
admin.site.register(Data_History)
admin.site.register(Info)
