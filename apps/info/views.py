from django.shortcuts import render


def index(request):
    # TODO change this url to your hostname
    context = {
        'current_site': "https://simpul.pythonanywhere.com"
    }
    return render(request, 'info/index.html', context)
