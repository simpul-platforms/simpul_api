from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.db import models


class Berita(models.Model):
    user_penulis = models.ForeignKey(
        get_user_model(),
        on_delete=models.CASCADE,
        )
    waktu = models.DateTimeField()
    judul = models.CharField(max_length=100)
    isi = models.TextField()

    def __str__(self):
        return self.judul

    class Meta:
        ordering = ('waktu',)


class Info(models.Model):
    foto_info = models.ImageField()
    grup_tujuan = models.ManyToManyField(Group,)


class Data_History(models.Model):
    user = models.CharField(max_length=150)
    aksi = models.CharField(max_length=20)
    detail = models.TextField()

    def __str__(self):
        return self.user + ' ' + self.aksi
