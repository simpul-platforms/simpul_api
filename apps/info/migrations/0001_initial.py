# Generated by Django 2.1.3 on 2018-11-14 22:31

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0009_alter_user_last_name_max_length'),
    ]

    operations = [
        migrations.CreateModel(
            name='Berita',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('waktu', models.DateTimeField()),
                ('judul', models.CharField(max_length=100)),
                ('isi', models.TextField()),
            ],
            options={
                'ordering': ('waktu',),
            },
        ),
        migrations.CreateModel(
            name='Data_History',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('user', models.CharField(max_length=150)),
                ('aksi', models.CharField(max_length=20)),
                ('detail', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Info',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('foto_info', models.ImageField(upload_to='')),
                ('grup_tujuan', models.ManyToManyField(to='auth.Group')),
            ],
        ),
    ]
